clc; clear; close all

% Load input frame
ptCloud = pcread('001001.pcd');

if length(size(ptCloud.Location)) == 2
    pt_location = ptCloud.Location;
else
    pt_location = reshape(ptCloud.Location, [size(ptCloud.Location,2),3]);
end
if size(ptCloud.Intensity,1) == 1
    pt_intensity = ptCloud.Intensity';
else
    pt_intensity = ptCloud.Intensity;
end

pt_intensity = pt_intensity / 255; % intensity [0, 1]

% downsample the pointcloud
skip = 150;
X = pt_location(1:skip:end,:);
y = double(pt_intensity(1:skip:end));

% Test points are all points
t = pt_location;

% measuring regression time
tic;

%% RVM
reg = rvm_regression(X, y);
reg.run;

%% OUTPUT VARIABLES
PARAMETER			= reg.PARAMETER;
HYPERPARAMETER		= reg.HYPERPARAMETER;

% Manipulate the returned weights for convenience later
w_infer						= zeros(size(X,1),1);
w_infer(PARAMETER.Relevant)	= PARAMETER.Value;

% Compute the inferred prediction function
basisWidth  = 1.5;
test_BASIS = exp(-distSquared(t,X) / (basisWidth^2));
y_out = test_BASIS * w_infer;
% map the output into [0,1]
f = (y_out + abs(min(y_out))) / (max(y_out) - min(y_out));

toc

% error 
error = pt_intensity - f;
disp(['error: ', num2str(sqrt(sum(error.^2)) / length(f))])

%% plot the output
cmap = single(f .* repmat([255, 255, 255], length(f), 1)/ 255);
newptcloud = pointCloud(t, 'Intensity', f, 'Color', cmap);

figure; hold on
pcshow(newptcloud)
plot3(X(PARAMETER.Relevant, 1), X(PARAMETER.Relevant, 2), X(PARAMETER.Relevant, 3),'rs')
title('RVM Intensity Regression')
axis equal, grid on

orig_cmap = single(pt_intensity .* repmat([255, 255, 255], length(pt_intensity), 1)/ 255);
orig_ptcloud = pointCloud(t, 'Intensity', pt_intensity, 'Color', orig_cmap);

figure; hold on
pcshow(orig_ptcloud)
title('Original Point Cloud')
axis equal, grid on