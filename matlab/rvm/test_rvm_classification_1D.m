clc; clear; close all
% test for rvm classification class. 1D 
% generate some data
N           = 900;	% Gives a nice square grid of decent size
iterations	= 500;
sqrtN		= floor(sqrt(N));
N			= sqrtN*sqrtN;
x			= [0:sqrtN-1]'/sqrtN;
[gx, gy]	= meshgrid(x);
X			= [gx(:) gy(:)];
% Heuristically adjust basis width to account for distance scaling with dimension.
dimension = size(X,2);
basisWidth = 0.05;
basisWidth	= basisWidth^(1 / dimension);		% NB: data is in [0,1]

% Compute squared exponential basis (design) matrix
BASIS       = exp(-distSquared(X,X) / (basisWidth^2));

% Randomise some weights, then make each weight sparse with probability
% pSparse 
pSparse		= 0.90;
M			= size(BASIS,2);
w			= randn(M,1) * 100 / (M * (1 - pSparse));
sparse		= rand(M,1) < pSparse;
w(sparse)	= 0;

% Now we have the basis and weights, compute linear model
z			= BASIS*w;

% Generate random [0,1] labels given by the log-odds 'z'
sigmoid = @(x) (1 ./ (1+exp(-x)));
Targets	= double(rand(N,1) < sigmoid(z));

%% RVM
classifier = rvm_classification(X, Targets);
classifier.run;

%% OUTPUT VARIABLES
PARAMETER			= classifier.PARAMETER;
HYPERPARAMETER		= classifier.HYPERPARAMETER;

% Manipulate the returned weights for convenience later
w_infer						= zeros(size(BASIS,2),1);
w_infer(PARAMETER.Relevant)	= PARAMETER.Value;

% Compute squared exponential basis (design) matrix
BASIS       = exp(-distSquared(X,X) / (basisWidth^2));
% Compute the inferred prediction function
y	= BASIS * w_infer;
y_l = double(sigmoid(y) > 0.5);


%% Plot the result
figure;
clf
whitebg(1,'k')
subplot(1,3,1)
plot3(X(:,1),X(:,2),Targets,'w.')
hold on
mesh(gx,gy,reshape(y_l,size(gx)),'edgecolor','r','facecolor','r')
hold off
light

% Compare the data and the predictive model (post link-function)
subplot(1,3,2)
mesh(gx,gy,reshape(z,size(gx)),'edgecolor','w','facecolor','w')
hold on
mesh(gx,gy,reshape(y,size(gx)),'edgecolor','r','facecolor','r')
hold off
light
legend('Actual','Model','Location','NorthWest')
legend('boxoff')

% Show the inferred weights
subplot(1,3,3)
h	= stem(w_infer,'filled');
set(h,'Markersize',3,'Color','r')
set(gca,'Xlim',[0 N+1])


