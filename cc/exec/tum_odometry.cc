#include <iostream>
#include <memory>
#include <random>

#include <pcl/point_cloud.h>
#include <pcl/common/transforms.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/registration/gicp.h>
#include <pcl/registration/gicp6d.h>
#include <pcl/registration/ndt.h>
#include <sophus/se3.hpp>
#include <tbb/tbb.h>

#include "common/timer/timer.h"
#include "registration/gicp.h"
#include "registration/gicp_rvm.h"
#include "exec/tum_devkit/tum_util.h"

namespace bi = library::bayesian_inference;
namespace tr = library::timer;

void ComputeOdometry ( int sequence_idx ) {

  std::ostringstream oss;
  oss << std::setw(2) << std::setfill('0') << sequence_idx;

  std::string sequences[6];
  sequences[0] = "/ssd-data/rgbd_slam/rgbd_dataset_freiburg1_desk";
  sequences[1] = "/ssd-data/rgbd_slam/rgbd_dataset_freiburg2_desk";
  sequences[2] = "/ssd-data/rgbd_slam/rgbd_dataset_freiburg3_nostructure_texture_far";
  sequences[3] = "/ssd-data/rgbd_slam/rgbd_dataset_freiburg3_nostructure_texture_near_withloop";
  sequences[4] = "/ssd-data/rgbd_slam/rgbd_dataset_freiburg1_xyz";
  sequences[5] = "/ssd-data/rgbd_slam/rgbd_dataset_freiburg1_rph";

  TUMData data(sequences[sequence_idx]);

  bi::GicpRvm::PointCloudPtr target_cloud;
  bi::GicpRvm::KdTreePtr target_kdtree;
  bi::GicpRvm::MatricesVectorPtr target_covs;
  bi::GicpRvm::RvmDecisionFunctionPtr target_rvm;


  Sophus::SE3d init_gicp;
  Sophus::SE3d toInitial_gicp;
  std::ofstream gicp_out("/ssd-data/rgbd_slam/results/"+oss.str()+"gicp_results.txt");
  std::ofstream gicp_traj("/ssd-data/rgbd_slam/results/"+oss.str()+"gicp_traj.txt");
  Sophus::SE3d init_rvm;
  Sophus::SE3d toInitial_rvm;
  std::ofstream rvm_out("/ssd-data/rgbd_slam/results/"+oss.str()+"rvm_results.txt");
  std::ofstream rvm_traj("/ssd-data/rgbd_slam/results/"+oss.str()+"rvm_traj.txt");
  Sophus::SE3d init_pcl;
  Sophus::SE3d toInitial_pcl;
  std::ofstream pcl_out("/ssd-data/rgbd_slam/results/"+oss.str()+"pcl6D_results.txt");
  std::ofstream pcl_traj("/ssd-data/rgbd_slam/results/"+oss.str()+"pcl6D_traj.txt");
  Sophus::SE3d init_ndt;
  Sophus::SE3d toInitial_ndt;
  std::ofstream ndt_out("/ssd-data/rgbd_slam/results/"+oss.str()+"ndt_results.txt");
  std::ofstream ndt_traj("/ssd-data/rgbd_slam/results/"+oss.str()+"ndt_traj.txt");
  Sophus::SE3d init_pose;
  std::ofstream init_traj("/ssd-data/rgbd_slam/results/"+oss.str()+"init_traj.txt");
  std::ofstream gt_out("/ssd-data/rgbd_slam/results/"+oss.str()+"gt.txt");
  int step = 1;
  for (int i = 0; i < data.Size()-step; i += step) {
    Sophus::SE3d target_pose = data.GetPoseAt(i);
    Sophus::SE3d source_pose = data.GetPoseAt(i+step);
    Sophus::SE3d gt_pose = target_pose.inverse() * source_pose;
    double source_time = data.GetTimeAt(i+step);
    //Sophus::SE3d gt_pose = target_pose*source_pose.inverse();
    std::cout << "GT Pose: \n";
    std::cout << gt_pose.matrix() << std::endl;
    gt_out << i << " " << i+step << " " <<gt_pose.log().norm() << " " << gt_pose.so3().log().norm() << " " << gt_pose.translation().norm() << std::endl;

    if( i == 0) {
      gicp_traj << std::setprecision(16)
        << data.GetTimeAt(0) << " "
        << toInitial_gicp.translation().x() << " "
        << toInitial_gicp.translation().y() << " "
        << toInitial_gicp.translation().z() << " "
        << toInitial_gicp.so3().unit_quaternion().x() << " "
        << toInitial_gicp.so3().unit_quaternion().y() << " "
        << toInitial_gicp.so3().unit_quaternion().z() << " "
        << toInitial_gicp.so3().unit_quaternion().w() << std::endl;
      rvm_traj << std::setprecision(16)
        << data.GetTimeAt(0) << " "
        << toInitial_gicp.translation().x() << " "
        << toInitial_gicp.translation().y() << " "
        << toInitial_gicp.translation().z() << " "
        << toInitial_gicp.so3().unit_quaternion().x() << " "
        << toInitial_gicp.so3().unit_quaternion().y() << " "
        << toInitial_gicp.so3().unit_quaternion().z() << " "
        << toInitial_gicp.so3().unit_quaternion().w() << std::endl;
      pcl_traj << std::setprecision(16)
        << data.GetTimeAt(0) << " "
        << toInitial_gicp.translation().x() << " "
        << toInitial_gicp.translation().y() << " "
        << toInitial_gicp.translation().z() << " "
        << toInitial_gicp.so3().unit_quaternion().x() << " "
        << toInitial_gicp.so3().unit_quaternion().y() << " "
        << toInitial_gicp.so3().unit_quaternion().z() << " "
        << toInitial_gicp.so3().unit_quaternion().w() << std::endl;
      ndt_traj << std::setprecision(16)
        << data.GetTimeAt(0) << " "
        << toInitial_gicp.translation().x() << " "
        << toInitial_gicp.translation().y() << " "
        << toInitial_gicp.translation().z() << " "
        << toInitial_gicp.so3().unit_quaternion().x() << " "
        << toInitial_gicp.so3().unit_quaternion().y() << " "
        << toInitial_gicp.so3().unit_quaternion().z() << " "
        << toInitial_gicp.so3().unit_quaternion().w() << std::endl;
      init_traj << std::setprecision(16)
        << data.GetTimeAt(0) << " "
        << init_pose.translation().x() << " "
        << init_pose.translation().y() << " "
        << init_pose.translation().z() << " "
        << init_pose.so3().unit_quaternion().x() << " "
        << init_pose.so3().unit_quaternion().y() << " "
        << init_pose.so3().unit_quaternion().z() << " "
        << init_pose.so3().unit_quaternion().w() << std::endl;
      target_cloud = data.GetPointCloudAt(i);
    }
    std::cout << "Target size: " << target_cloud->size() << std::endl;


    pcl::PointCloud<pcl::PointXYZI>::Ptr source_cloud = data.GetPointCloudAt(i+step);
    std::cout << "Source size: " << source_cloud->size() << std::endl;

    pcl::PointCloud<pcl::PointXYZI>::Ptr final_cloud(new pcl::PointCloud<pcl::PointXYZI>);

    tr::Timer t;
    pcl::NormalDistributionsTransform<pcl::PointXYZI, pcl::PointXYZI> ndt;
    ndt.setTransformationEpsilon (1e-4);
    // Setting maximum step size for More-Thuente line search.
    ndt.setStepSize (0.1);
    //Setting Resolution of NDT grid structure (VoxelGridCovariance).
    ndt.setResolution (1.0);
    ndt.setMaximumIterations(50);
    t.Start();
    ndt.setInputSource( source_cloud );
    ndt.setInputTarget( target_cloud );

    ndt.align(*final_cloud, Eigen::Matrix<float,4,4>::Identity());
    double time = t.GetSeconds();
    printf("Took %5.3f sec to align\n", time);

    init_ndt = Sophus::SE3d::fitToSE3(ndt.getFinalTransformation().cast<double>());
    Sophus::SE3d error = init_ndt.inverse()*gt_pose;
    ndt_out << i << " " << i+step << " " << error.log().norm() << " " << error.so3().log().norm() << " " << error.translation().norm() << " " << time << std::endl;
    toInitial_ndt = toInitial_ndt*init_ndt;
      ndt_traj << std::setprecision(16)
        << source_time << " "
        << toInitial_ndt.translation().x() << " "
        << toInitial_ndt.translation().y() << " "
        << toInitial_ndt.translation().z() << " "
        << toInitial_ndt.so3().unit_quaternion().x() << " "
        << toInitial_ndt.so3().unit_quaternion().y() << " "
        << toInitial_ndt.so3().unit_quaternion().z() << " "
        << toInitial_ndt.so3().unit_quaternion().w() << std::endl;


    pcl::GeneralizedIterativeClosestPoint6D pcl_gicp6d;
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr final_cloud6D(new pcl::PointCloud<pcl::PointXYZRGBA>);
    t.Start();
    pcl_gicp6d.setInputSource( data.GetRGBPointCloudAt(i+step) );
    pcl_gicp6d.setInputTarget( data.GetRGBPointCloudAt(i) );

    pcl_gicp6d.align(*final_cloud6D, Eigen::Matrix<float,4,4>::Identity());
    time = t.GetSeconds();
    printf("Took %5.3f sec to align\n", time);

    init_pcl = Sophus::SE3d::fitToSE3(pcl_gicp6d.getFinalTransformation().cast<double>());
    error = init_pcl.inverse()*gt_pose;
    pcl_out << i << " " << i+step << " " << error.log().norm() << " " << error.so3().log().norm() << " " << error.translation().norm() << std::endl;
    toInitial_pcl = toInitial_pcl*init_pcl;
      pcl_traj << std::setprecision(16)
        << source_time << " "
        << toInitial_pcl.translation().x() << " "
        << toInitial_pcl.translation().y() << " "
        << toInitial_pcl.translation().z() << " "
        << toInitial_pcl.so3().unit_quaternion().x() << " "
        << toInitial_pcl.so3().unit_quaternion().y() << " "
        << toInitial_pcl.so3().unit_quaternion().z() << " "
        << toInitial_pcl.so3().unit_quaternion().w() << std::endl;

    bi::GICP<pcl::PointXYZI> gicp;
    t.Start();
    gicp.setSourceCloud( source_cloud );
    gicp.setTargetCloud( target_cloud );

    gicp.align(final_cloud, Sophus::SE3d());
    time = t.GetSeconds();
    printf("Took %5.3f sec to align\n", time);

    init_gicp = gicp.getFinalTransformation();
    error = init_gicp.inverse()*gt_pose;
    gicp_out << i << " " << i+step << " " << error.log().norm() << " " << error.so3().log().norm() << " " << error.translation().norm() << " " << time << std::endl;
    toInitial_gicp = toInitial_gicp * init_gicp;
      gicp_traj << std::setprecision(16)
        << source_time << " "
        << toInitial_gicp.translation().x() << " "
        << toInitial_gicp.translation().y() << " "
        << toInitial_gicp.translation().z() << " "
        << toInitial_gicp.so3().unit_quaternion().x() << " "
        << toInitial_gicp.so3().unit_quaternion().y() << " "
        << toInitial_gicp.so3().unit_quaternion().z() << " "
        << toInitial_gicp.so3().unit_quaternion().w() << std::endl;

    bi::GicpRvm gicp_rvm;
    t.Start();
    gicp_rvm.setSourceCloud( source_cloud );
    if( i == 0 ) {
      gicp_rvm.setTargetCloud( target_cloud );
    } else {
      gicp_rvm.setTargetCloud( target_cloud, target_kdtree, target_covs, target_rvm);
    }

    gicp_rvm.align(final_cloud, Sophus::SE3d());
    time = t.GetSeconds();
    printf("Took %5.3f sec to align\n", time);

    init_rvm = gicp_rvm.getFinalTransformation();
    error = init_rvm.inverse()*gt_pose;
    rvm_out << i << " " << i+step << " " << error.log().norm() << " " << error.so3().log().norm() << " " << error.translation().norm() << " " << time << std::endl;
    toInitial_rvm = toInitial_rvm * init_rvm;
      rvm_traj << std::setprecision(16)
        << source_time << " "
        << toInitial_rvm.translation().x() << " "
        << toInitial_rvm.translation().y() << " "
        << toInitial_rvm.translation().z() << " "
        << toInitial_rvm.so3().unit_quaternion().x() << " "
        << toInitial_rvm.so3().unit_quaternion().y() << " "
        << toInitial_rvm.so3().unit_quaternion().z() << " "
        << toInitial_rvm.so3().unit_quaternion().w() << std::endl;

    gicp_rvm.getSourceCloud(target_cloud, target_kdtree, target_covs, target_rvm);

      init_traj << std::setprecision(16)
        << source_time << " "
        << init_pose.translation().x() << " "
        << init_pose.translation().y() << " "
        << init_pose.translation().z() << " "
        << init_pose.so3().unit_quaternion().x() << " "
        << init_pose.so3().unit_quaternion().y() << " "
        << init_pose.so3().unit_quaternion().z() << " "
        << init_pose.so3().unit_quaternion().w() << std::endl;
  }
}

int main() {

  tbb::task_scheduler_init init(4);
  tbb::parallel_for<int>( 0, 4, 1, ComputeOdometry );
  //ComputeOdometry(4);

  return 0;
}
