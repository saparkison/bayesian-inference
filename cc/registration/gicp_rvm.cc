#include <iostream>

#include <ceres/gradient_checker.h>
#include <ceres/ceres.h>
#include <Eigen/StdVector>
#include <pcl/common/transforms.h>

#include "local_parameterization_se3.h"
#include "gicp_rvm.h"

namespace library {
namespace bayesian_inference {

void GicpRvm::align(PointCloudPtr finalCloud) {
    Eigen::Matrix4d mat = Eigen::Matrix4d::Identity();
    Sophus::SE3d init(mat);
    align(finalCloud, init);
};

void GicpRvm::align(
        PointCloudPtr finalCloud, const Sophus::SE3d &initTransform) {


    Sophus::SE3d currentTransform(initTransform);
    bool converged = false;
    size_t count = 0;

    while(converged!=true) {
        auto start = std::chrono::high_resolution_clock::now();
        std::vector<Sophus::SE3d> transformsVec;
        CovarianceVector covVec;

        double mseHigh = 0;

        typename pcl::PointCloud<pcl::PointXYZI>::Ptr transformedSource (new pcl::PointCloud<pcl::PointXYZI>());
        Sophus::SE3d transform = currentTransform;
        Eigen::Matrix4d transMat = transform.matrix();
        pcl::transformPointCloud(*sourceCloud_,
                                    *transformedSource,
                                    transMat);

        std::vector<int> targetIndx;
        std::vector<float> distSq;
        PointCloudPtr aligned_target_cloud(new PointCloud());
        MatricesVectorPtr aligned_target_cov = std::make_shared<MatricesVector>();

        std::cout << "Num Points: " << transformedSource->size() << std::endl;
        for(int sourceIndx = 0; sourceIndx != transformedSource->size(); sourceIndx++) {
            const pcl::PointXYZI &transformedSourcePoint = transformedSource->points[sourceIndx];

            targetKdTree_->nearestKSearch(transformedSourcePoint, 1, targetIndx, distSq);

            aligned_target_cloud->push_back(targetCloud_->at(targetIndx[0]));
            aligned_target_cov->push_back(targetCovariances_->at(targetIndx[0]));

        } // For loop over points

        // Build The Problem
        ceres::GradientProblem problem(new GicpRvmCostFunction (sourceCloud_, aligned_target_cloud,
            sourceCovariances_, aligned_target_cov,
            source_rvm_, target_rvm_), new LocalParameterizationSE3());


        // Sovler Options
        ceres::GradientProblemSolver::Options options;
        options.gradient_tolerance = 0.1 * Sophus::Constants<float>::epsilon();
        options.function_tolerance = 0.1 * Sophus::Constants<float>::epsilon();

        // Add Sophus SE3 Parameter block with local parametrization
        Sophus::SE3d estTransform(currentTransform);

        // Solve
        ceres::GradientProblemSolver::Summary summary;
        ceres::Solve(options, problem, estTransform.data(), &summary);
        std::cout << summary.FullReport() << std::endl;

        auto end = std::chrono::high_resolution_clock::now();
        double durration = std::chrono::duration<double, std::micro>(end-start).count();
        convergeTime.push_back(durration);
        convergeTransforms.push_back(estTransform.matrix().cast<float>());

        double mse = (currentTransform.inverse()*estTransform).log().squaredNorm();
        if(mse < 1e-4 || count>50)
            converged = true;
        std::cout<< "MSE: " << mse << std::endl;
        std::cout<< "Transform: " << std::endl;
        std::cout<< estTransform.matrix() << std::endl;
        std::cout<< "Itteration: " << count << std::endl;
        currentTransform = estTransform;
        count++;
    }

    finalTransformation_ = currentTransform;

    Sophus::SE3d trans = finalTransformation_*baseTransformation_;
    Eigen::Matrix4f mat = (trans.matrix()).cast<float>();
    if( finalCloud != nullptr ) {
        pcl::transformPointCloud(*sourceCloud_,
                                 *finalCloud,
                                 mat);
    }

    outer_iter = count;
};

void GicpRvm::computeCovariances(const PointCloudPtr cloudptr,
                                      KdTreePtr treeptr, MatricesVectorPtr matvecptr) {

    // Variables for computing Covariances
    Eigen::Vector3d mean;
    std::vector<int> nn_idecies; nn_idecies.reserve (kCorrespondences_);
    std::vector<float> nn_dist_sq; nn_dist_sq.reserve (kCorrespondences_);

    // Set up Itteration
    matvecptr->resize(cloudptr->size());

    for(size_t itter = 0; itter < cloudptr->size(); itter++) {
        const auto &query_pt = (*cloudptr)[itter];

        Eigen::Matrix3d cov;
        cov.setZero();
        mean.setZero();

        treeptr->nearestKSearch(query_pt, kCorrespondences_, nn_idecies, nn_dist_sq);

        for( int index: nn_idecies) {
            const auto &pt = (*cloudptr)[index];

            mean[0] += pt.x;
            mean[1] += pt.y;
            mean[2] += pt.z;

            cov(0,0) += pt.x*pt.x;

            cov(1,0) += pt.y*pt.x;
            cov(1,1) += pt.y*pt.y;

            cov(2,0) += pt.z*pt.x;
            cov(2,1) += pt.z*pt.y;
            cov(2,2) += pt.z*pt.z;
        }

        mean /= static_cast<double> (kCorrespondences_);
        for (int k = 0; k < 3; k++) {
            for (int l =0; l <= k; l++) {
                cov(k,l) /= static_cast<double> (kCorrespondences_);
                cov(k,l) -= mean[k]*mean[l];
                cov(l,k) = cov(k,l);
            }
        }

        // SVD decomposition for PCA
        Eigen::JacobiSVD<Eigen::Matrix3d> svd(cov, Eigen::ComputeFullU);
        cov.setZero();
        Eigen::Matrix3d U = svd.matrixU();

        for (int k = 0; k<3; k++) {
            Eigen::Vector3d col = U.col(k);
            double v = 1.;
            if (k == 2)
                v = epsilon_;
            cov+= v*col*col.transpose();
        }
        (*matvecptr)[itter] = cov;
    }
}

bool GicpRvm::GicpRvmCostFunction::Evaluate(const double* parameters, double* cost, double* gradient) const {
  Eigen::Map<Sophus::SE3<double> const> const transform(parameters);
  Eigen::Matrix3d R = transform.rotationMatrix();

  cost[0] = 0;

  if(gradient!=NULL) {
    std::fill(gradient, gradient + 7, 0);
  }

  for( size_t i = 0; i < source_cloud_->size(); i++) {
    const auto& source_p = source_cloud_->at(i);
    const auto& source_cov = source_covs_->at(i);
    const auto& target_p = target_cloud_->at(i);
    const auto& target_cov = target_covs_->at(i);

    Eigen::Matrix3d M = (target_cov+R*source_cov*R.transpose()).inverse();

    Eigen::Vector3d source_vec = source_p.getVector3fMap().cast<double>();
    Eigen::Vector3d transformed_p = transform*source_vec;
    Eigen::Vector3d res = target_p.getVector3fMap().cast<double>() - transformed_p;
    Eigen::Vector3d dT = M*res;

    double target_label = target_rvm_->PredictLabel(transformed_p);
    double source_label = source_rvm_->PredictLabel(source_vec);
    double regularizer = target_label - source_label;
    double reg_sq = lambda_ * std::pow(regularizer,2);

    cost[0] += std::pow(alpha_,2)*std::log(1.0+res.dot(dT)/std::pow(alpha_,2)) + reg_sq;

    if(gradient != NULL ) {

      double dalpha = std::pow(alpha_,2)/(res.dot(dT)+std::pow(alpha_,2));

      Eigen::Matrix3d Ta = (target_cov.transpose()
        + R* source_cov.transpose()*R.transpose()).inverse();
      Eigen::Vector3d tb = M*res;
      Eigen::Vector3d tc = Ta*res;

      Eigen::Matrix3d dR = -(tb*source_vec.transpose() +
            tc*(res.transpose()*Ta*R*source_cov.transpose()) +
            tb*(res.transpose()*M*R*source_cov) +
            tc*source_vec.transpose());

      /* MGJ: what about this simplified form?
      Eigen::Matrix dR = -2*tb*(source_vec.transpose() +
            res.transpose()*M*R*source_cov))
      */

      /* MGJ: Gradient of the regularizer. This is just a guid, the references and 
      syntax might not be correct. We need to get the basis points and coefficient
      of the trained RVM, plus calling the kernel directly. I think they're not public,
      we need to edit the RVM class. See right after Eq. (7).
      */

      //auto ell_inv = 1 / target_rvm_->kernel_length_scale; // we use scalar length scale (isotropic kernel) for now).

      Eigen::Vector3d dreg_t;
      Eigen::Matrix3d dreg_R;
      target_rvm_->ComputeGradient(transformed_p, dreg_t, dreg_R);
      dreg_t *= lambda_*(-2.0)*(regularizer);
      dreg_R *= lambda_*(-2.0)*(regularizer);

      dT *= -2.0*dalpha;
      dT += dreg_t;
      //auto dq = dRtodq(dalpha*dR+dreg_R, transform.unit_quaternion(), R);
      auto dq = dRtodq(dR, transform.unit_quaternion(), R);
      auto dreq_q  = dRtodq(dreg_R, transform.unit_quaternion(), R);
      gradient[4] += dT.x();
      gradient[5] += dT.y();
      gradient[6] += dT.z();
      gradient[0] += dalpha*dq.x() + dreq_q.x();
      gradient[1] += dalpha*dq.y() + dreq_q.y();
      gradient[2] += dalpha*dq.z() + dreq_q.z();
      gradient[3] += dalpha*dq.w() + dreq_q.w();
    }
  }
  if(gradient!=NULL && false) {
    double p[7];
    for(int n = 0; n<7; n++) {
      std::copy(parameters, parameters+7, p);
      p[n]+=1e-9;
      double c[1];
      Evaluate(p, c, NULL);
      std::cout << "Parameter " << n << " Numeric: " << (c[0]-cost[0])/1e-9 << " Analytical: " << gradient[n] <<std::endl;
    }
  }
  return true;
}

Eigen::Quaterniond GicpRvm::GicpRvmCostFunction::dRtodq(const Eigen::Matrix3d& dR,
                                                        const Eigen::Quaterniond& q,
                                                        const Eigen::Matrix3d& R)  const {
  Eigen::Quaterniond out;
  const double tx  = double(2)*q.x();
  const double ty  = double(2)*q.y();
  const double tz  = double(2)*q.z();
  const double tw  = double(2)*q.w();
  const double mfx  = double(-2)*tx;
  const double mfy  = double(-2)*ty;
  const double mfz  = double(-2)*tz;
  const double mtw  = double(-1)*tw;

  /* Eigen Quat to Rot
  res.coeffRef(0,0) = Scalar(1)-(tyy+tzz);
  res.coeffRef(0,1) = txy-twz;
  res.coeffRef(0,2) = txz+twy;
  res.coeffRef(1,0) = txy+twz;
  res.coeffRef(1,1) = Scalar(1)-(txx+tzz);
  res.coeffRef(1,2) = tyz-twx;
  res.coeffRef(2,0) = txz-twy;
  res.coeffRef(2,1) = tyz+twx;
  res.coeffRef(2,2) = Scalar(1)-(txx+tyy);
  */


  Eigen::Matrix3d dRdw;
  dRdw(0,0) = double(0);
  dRdw(0,1) = -tz;
  dRdw(0,2) = ty;
  dRdw(1,0) = tz;
  dRdw(1,1) = double(0);
  dRdw(1,2) = -tx;
  dRdw(2,0) = -ty;
  dRdw(2,1) = tx;
  dRdw(2,2) = double(0);

  out.w() = (dR.transpose()*dRdw).trace();

  Eigen::Matrix3d dRdx;
  dRdx(0,0) = double(0);
  dRdx(0,1) = ty;
  dRdx(0,2) = tz;
  dRdx(1,0) = ty;
  dRdx(1,1) = mfx;
  dRdx(1,2) = mtw;
  dRdx(2,0) = tz;
  dRdx(2,1) = tw;
  dRdx(2,2) = mfx;

  out.x() = (dR.transpose()*dRdx).trace();

  Eigen::Matrix3d dRdy;
  dRdy(0,0) = mfy;
  dRdy(0,1) = tx;
  dRdy(0,2) = tw;
  dRdy(1,0) = tx;
  dRdy(1,1) = double(0);
  dRdy(1,2) = tz;
  dRdy(2,0) = mtw;
  dRdy(2,1) = tz;
  dRdy(2,2) = mfy;

  out.y() = (dR.transpose()*dRdy).trace();

  Eigen::Matrix3d dRdz;
  dRdz(0,0) = mfz;
  dRdz(0,1) = mtw;
  dRdz(0,2) = tx;
  dRdz(1,0) = tw;
  dRdz(1,1) = mfz;
  dRdz(1,2) = ty;
  dRdz(2,0) = tx;
  dRdz(2,1) = ty;
  dRdz(2,2) = double(0);

  out.z() = (dR.transpose()*dRdz).trace();

  return out;
}


} // namespace library
} // namespace bayesian_inference
