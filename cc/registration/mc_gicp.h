#pragma once

#include <pcl/registration/icp.h>
#include <pcl/point_representation.h>
#include <pcl/kdtree/impl/kdtree_flann.hpp>
#include <Eigen/Geometry>
#include <sophus/common.hpp>
#include <sophus/se3.hpp>
#include <sophus/types.hpp>

#include "registration/point_types.h"

namespace library {
namespace bayesian_inference {

template <size_t N>
class MultiChannelGICP {
 public:
  typedef PointXYZP<N> PointT;
  typedef pcl::PointCloud<PointT> PointCloud;
  typedef typename PointCloud::Ptr PointCloudPtr;

  typedef std::vector<Eigen::Matrix3d,
                      Eigen::aligned_allocator<Eigen::Matrix3d>>
      MatricesVector;

  typedef std::shared_ptr<MatricesVector> MatricesVectorPtr;
  typedef std::shared_ptr<const MatricesVector> MatricesVectorConstPtr;

  typedef pcl::KdTreeFLANN<PointT> KdTree;
  typedef typename KdTree::Ptr KdTreePtr;

  typedef Eigen::Matrix<double, 6, 1> Vector6d;

  std::vector<Eigen::Matrix4f, Eigen::aligned_allocator<Eigen::Matrix4f> >
      convergeTransforms;
  std::vector<double> convergeTime;

  MultiChannelGICP(const float (&alpha)[3+N], const Eigen::Matrix<float,N,N>& lambda, int k = 8, double epsilon = 1e-12)
      : kCorrespondences_(k), epsilon_(epsilon) {
    Eigen::Matrix4d mat = Eigen::Matrix4d::Identity();
    baseTransformation_ = Sophus::SE3d(mat);

    std::copy(alpha, alpha+3+N, alpha_);
    point_rep_.setRescaleValues (alpha_);
    lambda_inv_ = lambda.inverse();
  };

  inline void SetInputSource(const PointCloudPtr &cloud) {
    sourceCloud_ = cloud;
    sourceKdTree_ = KdTreePtr(new KdTree());
    sourceKdTree_->setInputCloud(sourceCloud_);
    sourceCovariances_ = MatricesVectorPtr(new MatricesVector());
  };

  inline void SetInputTarget(const PointCloudPtr &cloud) {
    targetCloud_ = cloud;
    targetKdTree_ = KdTreePtr(new KdTree());
    targetKdTree_->setInputCloud(targetCloud_);
    targetCovariances_ = MatricesVectorPtr(new MatricesVector());
    // ...and build multidemensional-tree
    target_tree_mc_.setInputCloud (targetCloud_);
    target_tree_mc_.setPointRepresentation (
      boost::make_shared<MyPointRepresentation> (point_rep_));
  };

  void Align(PointCloudPtr finalCloud);

  void Align(PointCloudPtr finalCloud, const Sophus::SE3d &initTransform);

  Sophus::SE3d GetFinalTransformation() {
    Sophus::SE3d temp = finalTransformation_;
    return temp;
  };

  int GetOuterIter() { return outer_iter; }

 protected:
  int kCorrespondences_;
  double epsilon_;
  double translationEpsilon_;
  double rotationEpsilon_;
  int maxInnerIterations_;

  int outer_iter;

  float alpha_[3+N];
  Eigen::Matrix<float,N,N> lambda_inv_;

  Sophus::SE3d baseTransformation_;
  Sophus::SE3d finalTransformation_;

  PointCloudPtr sourceCloud_;
  KdTreePtr sourceKdTree_;
  MatricesVectorPtr sourceCovariances_;

  PointCloudPtr targetCloud_;
  KdTreePtr targetKdTree_;
  MatricesVectorPtr targetCovariances_;

  pcl::KdTreeFLANN<PointT> target_tree_mc_;

  void ComputeCovariances(const PointCloudPtr cloudptr, KdTreePtr treeptr,
                          MatricesVectorPtr matvec);
          /**  \brief Custom point representation to perform kdtree searches in more than 3 (i.e. in all 6) dimensions. */
  class MyPointRepresentation : public pcl::PointRepresentation<PointT>
    {
      using pcl::PointRepresentation<PointT>::nr_dimensions_;
      using pcl::PointRepresentation<PointT>::trivial_;

      public:
        typedef boost::shared_ptr<MyPointRepresentation> Ptr;
        typedef boost::shared_ptr<const MyPointRepresentation> ConstPtr;

        MyPointRepresentation ()
        {
          nr_dimensions_ = 3+N;
          trivial_ = false;
        }

        ~MyPointRepresentation ()
        {
        }

        inline Ptr
        makeShared () const
        {
          return Ptr (new MyPointRepresentation (*this));
        }

        void
        copyToFloatArray (const PointT &p, float * out) const override
        {
          // copy all of the six values
          out[0] = p.x;
          out[1] = p.y;
          out[2] = p.z;
          for(int it = 0; it < N; it++) {
            out[3+it] = p.probability[it];
          }
        }
      };

      /** \brief Enables 6d searches with kd-tree class using the color weight. */
      MyPointRepresentation point_rep_;
};

}  // namespace bayesian_inference
}  // namespace library

#include "registration/mc_gicp.hpp"
