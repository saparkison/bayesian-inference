#pragma once

#include <iostream>
#include <chrono>
#include <cmath>

#include <ceres/gradient_checker.h>
#include <ceres/ceres.h>
#include <Eigen/StdVector>
#include <pcl/common/transforms.h>

#include "registration/gicp_cost_function.h"
#include "registration/local_parameterization_se3.h"
#include "registration/sqloss.h"


namespace library {
namespace bayesian_inference {

template <size_t N>
void MultiChannelGICP<N>::Align(PointCloudPtr finalCloud) {
    Eigen::Matrix4d mat = Eigen::Matrix4d::Identity();
    Sophus::SE3d init(mat);
    align(finalCloud, init);
};

template <size_t N>
void MultiChannelGICP<N>::Align(
        PointCloudPtr finalCloud, const Sophus::SE3d &initTransform) {

    ComputeCovariances(sourceCloud_, sourceKdTree_, sourceCovariances_);
    ComputeCovariances(targetCloud_, targetKdTree_, targetCovariances_);

    Sophus::SE3d currentTransform(initTransform);
    bool converged = false;
    size_t count = 0;

    while(converged!=true) {
        auto start = std::chrono::high_resolution_clock::now();
        std::vector<Sophus::SE3d> transformsVec;
        MatricesVector covVec;

        // Build The Problem
        ceres::Problem problem;

        // Add Sophus SE3 Parameter block with local parametrization
        Sophus::SE3d estTransform(currentTransform);
        problem.AddParameterBlock(estTransform.data(), Sophus::SE3d::num_parameters,
                                  new LocalParameterizationSE3);

        double mseHigh = 0;

        typename pcl::PointCloud<PointT>::Ptr transformedSource (new pcl::PointCloud<PointT>());
        Sophus::SE3d transform = currentTransform;
        Eigen::Matrix4d transMat = transform.matrix();
        pcl::transformPointCloud(*sourceCloud_,
                                    *transformedSource,
                                    transMat);

        std::vector<int> targetIndx;
        std::vector<float> distSq;


        std::cout << "Num Points: " << transformedSource->size() << std::endl;
        for(int sourceIndx = 0; sourceIndx != transformedSource->size(); sourceIndx++) {
            const PointT &transformedSourcePoint = transformedSource->points[sourceIndx];

            target_tree_mc_.nearestKSearch(transformedSourcePoint, 1, targetIndx, distSq);
                if( distSq[0] < 8.0 ) {
                    PointT &sourcePoint =
                        sourceCloud_->points[sourceIndx];
                    const Eigen::Matrix3d &sourceCov =
                        sourceCovariances_->at(sourceIndx);
                    PointT &targetPoint =
                        targetCloud_->points[targetIndx[0]];
                    const Eigen::Matrix3d &targetCov =
                        targetCovariances_->at(targetIndx[0]);

                    //std::cout << "Dist " << (targetPoint.cuVector3fMap()-sourcePoint.cuVector3fMap()).norm() << std::endl;

                    //   Autodif Cost function
                    //GICPCostFunctorAutoDiff *c= new GICPCostFunctorAutoDiff(sourcePoint,
                    //                                                       targetPoint,
                    //                                                       sourceCov,
                    //                                                       targetCov,
                    //                                                       baseTransformation_);
                    //ceres::CostFunction* cost_function =
                    //    new ceres::AutoDiffCostFunction<GICPCostFunctorAutoDiff,
                    //                                    1,
                    //                                    Sophus::SE3d::num_parameters>(c);

                    //   Analytical Cost Function
                    auto* cost_function = new GICPCostFunction<PointT>(sourcePoint,
                                                                       targetPoint,
                                                                       sourceCov,
                                                                       targetCov,
                                                                       baseTransformation_);

                    problem.AddResidualBlock(cost_function,
                                               new SQLoss(),
                                             estTransform.data());
                    // Gradient Check
                    if (false) {
                        ceres::NumericDiffOptions numeric_diff_options;
                        numeric_diff_options.relative_step_size = 1e-13;

                        std::vector<const ceres::LocalParameterization*> lp;
                        lp.push_back(new LocalParameterizationSE3);

                        ceres::GradientChecker gradient_checker(cost_function,
                                    &lp,
                                    numeric_diff_options);

                        ceres::GradientChecker::ProbeResults results;
                        std::vector<double *> params;
                        params.push_back(estTransform.data());
                        if (!gradient_checker.Probe(params.data(), 5e-4, &results)) {
                                std::cout << "An error has occurred:\n";
                                std::cout << results.error_log;
                                std::cout << results.jacobians[0] << std::endl;
                                std::cout << results.numeric_jacobians[0] << std::endl;
                                std::cout << estTransform.matrix() << std::endl;
                                std::cout << sourcePoint << std::endl;
                                std::cout << targetPoint << std::endl;
                                std::cout << sourceCov << std::endl;
                                std::cout << targetCov << std::endl;

                        }
                    }

                }

        } // For loop over points
        // Sovler Options
        ceres::Solver::Options options;
        options.gradient_tolerance = Sophus::Constants<double>::epsilon();
        options.function_tolerance = Sophus::Constants<double>::epsilon();
        options.linear_solver_type = ceres::DENSE_QR;
        options.num_threads = 1;
        options.max_num_iterations = 400;
       // options.check_gradients = true;
        options.gradient_check_numeric_derivative_relative_step_size = 1e-8;
        options.gradient_check_relative_precision = 1e-6;

        // Solve
        ceres::Solver::Summary summary;
        ceres::Solve(options, &problem, &summary);
        std::cout << summary.FullReport() << std::endl;

        auto end = std::chrono::high_resolution_clock::now();
        double durration = std::chrono::duration<double, std::micro>(end-start).count();
        convergeTime.push_back(durration);
        convergeTransforms.push_back(estTransform.matrix().cast<float>());

        double mse = (currentTransform.inverse()*estTransform).log().squaredNorm();
        if(mse < 1e-4 || count>50)
            converged = true;
        std::cout<< "MSE: " << mse << std::endl;
        std::cout<< "Transform: " << std::endl;
        std::cout<< estTransform.matrix() << std::endl;
        std::cout<< "Itteration: " << count << std::endl;
        currentTransform = estTransform;
        count++;
    }

    finalTransformation_ = currentTransform;

    Sophus::SE3d trans = finalTransformation_*baseTransformation_;
    Eigen::Matrix4f mat = (trans.matrix()).cast<float>();
    if( finalCloud != nullptr ) {
        pcl::transformPointCloud(*sourceCloud_,
                                 *finalCloud,
                                 mat);
    }

    outer_iter = count;
};

template <size_t N>
void MultiChannelGICP<N>::ComputeCovariances(const PointCloudPtr cloudptr,
                                      KdTreePtr treeptr, MatricesVectorPtr matvecptr) {

    // Variables for computing Covariances
    Eigen::Vector3f mean;
    std::vector<int> nn_idecies; nn_idecies.reserve (kCorrespondences_);
    std::vector<float> nn_dist_sq; nn_dist_sq.reserve (kCorrespondences_);

    // Set up Itteration
    matvecptr->resize(cloudptr->size());

    for(size_t itter = 0; itter < cloudptr->size(); itter++) {
       PointT &query_pt = (*cloudptr)[itter];

        Eigen::Matrix3f cov;
        cov.setZero();
        mean.setZero();

        treeptr->nearestKSearch(query_pt, kCorrespondences_, nn_idecies, nn_dist_sq);

        for( int index: nn_idecies) {
            PointT &pt = (*cloudptr)[index];

            mean += pt.ConstVector3fMap();
            cov += pt.ConstVector3fMap()*pt.ConstVector3fMap().transpose();
        }

        mean /= static_cast<float> (kCorrespondences_);
        cov = cov/static_cast<float>(kCorrespondences_) - mean*mean.transpose()+ (Eigen::Matrix3f::Identity()*0.0001);

        // SVD decomposition for PCA
        Eigen::JacobiSVD<Eigen::Matrix3f> svd(cov, Eigen::ComputeFullU | Eigen::ComputeFullV);
        Eigen::Matrix3f U = svd.matrixU();
        Eigen::Matrix3f V = svd.matrixV();
        Eigen::Matrix<float,2,3> U_sub = U.block<3,2>(0,0).transpose();


        // Multichannel mean and cov computation
        Eigen::Vector2f mean_mc = Eigen::Vector2f::Zero();
        Eigen::Matrix2f cov_mc = Eigen::Matrix2f::Zero();
        float lambda_tot = 0;
        Eigen::Vector2f mean_w = Eigen::Vector2f::Zero();
        Eigen::Matrix2f cov_w = Eigen::Matrix2f::Zero();
        for( int index: nn_idecies) {
          PointT &pt = (*cloudptr)[index];
          Eigen::Matrix<float, N, 1> res = pt.ConstProbabilityMap()-query_pt.ConstProbabilityMap();
          float lambda = std::exp( -1.f/2.f* res.dot(lambda_inv_*res));

          lambda_tot += lambda;
          Eigen::Vector2f temp = U_sub*pt.ConstVector3fMap();

          mean_mc += lambda*temp;
          cov_mc += lambda*temp*temp.transpose();
          mean_w += temp;
          cov_w += temp*temp.transpose();
        }
        mean_mc/=lambda_tot;
        cov_mc = cov_mc/lambda_tot - mean_mc *mean_mc.transpose()+(Eigen::Matrix2f::Identity()*0.0001);

        mean_w /= static_cast<float>(kCorrespondences_);
        cov_w = cov_w/static_cast<float>(kCorrespondences_) - mean_w *mean_w.transpose()+(Eigen::Matrix2f::Identity()*0.01);
        //Determine matrix sqrt
        Eigen::SelfAdjointEigenSolver<Eigen::Matrix2f> es(cov_w);
        Eigen::Matrix2f cov_p_sqrt = es.operatorInverseSqrt();

        Eigen::Matrix3f new_cov;
        new_cov.setZero();
        new_cov.block<2,2>(0,0) = cov_p_sqrt*cov_mc*cov_p_sqrt.transpose();
        new_cov(2,2) = epsilon_;

        (*matvecptr)[itter] = (U*new_cov*V.transpose()).cast<double>() + (Eigen::Matrix3d::Identity()*0.0001);
        //if(std::isnan((*matvecptr)[itter](0,0))) {
        //  std::cout << "Initial cov\n" << cov << std::endl;
        //  std::cout << "MC cov\n" << cov_mc << std::endl;
        //  std::cout << "W cov\n" << cov_w << std::endl;
        //  std::cout << "MC cov sqrt inv \n" << cov_p_sqrt << std::endl;
        //  std::cout << "New cov\n" << new_cov << std::endl;
        //  std::cout << "Final cov\n" << (*matvecptr)[itter] << std::endl;
        //}
    }

}

}  // namespace bayesian_inference
}  // namespace library
