#pragma once

#define PCL_NO_PRECOMPILE

#include <pcl/point_types.h>
#include <Eigen/Core>

namespace library {
namespace bayesian_inference {

template <size_t N>
struct PointXYZP : public pcl::_PointXYZ {
  float probability[N];
  PointXYZP(const pcl::_PointXYZ &p) {
    x = p.x;
    y = p.y;
    z = p.z;
    data[3] = 1.0f;
    for (size_t i = 0; i < N; i++) {
      probability[i] = 0;
    }
  }

  PointXYZP(const PointXYZP<N> &p) {
    x = p.x;
    y = p.y;
    z = p.z;
    data[3] = 1.0f;
    for (size_t i = 0; i < N; i++) {
      probability[i] = p.probability[i];
    }
  }

  PointXYZP() {
    x = y = z = 0.0f;
    data[3] = 1.0f;
    for (size_t i = 0; i < N; i++) {
      probability[i] = 0;
    }
  }

  PointXYZP(const float &_x,
            const float &_y,
            const float &_z) {
    x = _x;
    y = _y;
    z = _z;
    data[3] = 1.0f;
    for (size_t i = 0; i < N; i++) {
      probability[i] = 0;
    }
  }

  Eigen::Map<Eigen::Vector3f>
  Vector3fMap() {
    return (Eigen::Vector3f::Map(data));
  }

  Eigen::Map<const Eigen::Vector3f>
  ConstVector3fMap() {
    return (Eigen::Map<const Eigen::Vector3f>(data));
  }

  Eigen::Map<Eigen::Matrix<float, N, 1>>
  ProbabilityMap() {
    return (Eigen::Map<Eigen::Matrix<float, N, 1>>(probability));
  }

  Eigen::Map<const Eigen::Matrix<float, N, 1>>
  ConstProbabilityMap() {
    return (Eigen::Map<const Eigen::Matrix<float, N, 1>>(probability));
  }
};

}  // namespace bayesian_inference
}  // namespace library
