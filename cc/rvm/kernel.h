#pragma once

#include <vector>

#include <boost/assert.hpp>

#include <Eigen/Core>

namespace library {
namespace bayesian_inference {

template <class T>
class IKernel {
 public:
  virtual double Compute(const T &sample, const T &x_m, bool debug = false) const = 0;
  virtual double GetGradient() const = 0;
};

template <class T>
class GaussianKernel : public IKernel<T> {
 public:
  GaussianKernel(double r) : radius_sq_(r*r) {}

  virtual double Compute(const T &sample, const T &x_m, bool debug = false) const {
    if(debug)
      std::cout << "Compute\n";
    BOOST_ASSERT(sample.size() == x_m.size());

    if(debug)
      std::cout << "Sq n " << (x_m-sample).squaredNorm() << std::endl;;
    double sq_n = (x_m - sample).squaredNorm();

    if(debug)
      std::cout << "exp " << std::exp(-sq_n/radius_sq_) << std::endl;
    return std::exp(-sq_n / radius_sq_);
  }

  virtual double GetGradient() const {
    return radius_sq_;
  }

 private:
  double radius_sq_;
};

} // namespace bayesian_inference
} // namespace library
