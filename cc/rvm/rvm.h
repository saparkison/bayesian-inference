#pragma once

#include <algorithm>
#include <list>
#include <tuple>
#include <vector>

#include <boost/assert.hpp>
#include <ceres/ceres.h>
#include <Eigen/Core>
#include <Eigen/Sparse>
#include <Eigen/OrderingMethods>
#include <Eigen/SparseCholesky>

#include "common/timer/timer.h"

#include "rvm/kernel.h"

namespace tr = library::timer;

namespace library {
namespace bayesian_inference {

template <class T> class Rvm;

template <class T>
class RvmWeightFunction : public ceres::FirstOrderFunction {
 public:
  RvmWeightFunction(const Rvm<T> &rvm) : rvm_(rvm) {}

  virtual bool Evaluate(const double* parameters, double* cost, double* gradient) const {
    Eigen::MatrixXd w(rvm_.NumRelevanceVectors(), 1);
    for (int i=0; i<w.rows(); i++) {
      w(i, 0) = parameters[i];
    }

    Eigen::MatrixXd gradient_analytical;

    double ll = rvm_.ComputeLogLikelihood(w, &gradient_analytical);
    cost[0] = -ll;

    if (gradient != nullptr) {
      //double step = 1e-6;
      for (int i=0; i<w.rows(); i++) {
        //Eigen::MatrixXd w1 = w;
        //w1(i, 0) += step;

        //double ll1 = rvm_.ComputeLogLikelihood(w1);
        //gradient[i] = -(ll1 - ll) / (step);
        //printf("grad %d = %f vs %f\n", i, gradient[i], -gradient_analytical(i, 0));
        gradient[i] = -gradient_analytical(i, 0);
      }
    }

    return true;
  }

  virtual int NumParameters() const {
    return rvm_.NumRelevanceVectors();
  }

 private:
  const Rvm<T> &rvm_;

};

template <class T>
class Rvm {
  public:
    virtual double ComputeLogLikelihood(const Eigen::MatrixXd &w, Eigen::MatrixXd *gradient = nullptr) const = 0;
    virtual size_t NumRelevanceVectors() const = 0;
};

template <class T>
class RvmClassifier: public Rvm<T> {
 public:
  // data is n_samples X dimension
  // labels is n_samples X 1
  //Rvm(const Eigen::MatrixXd &data, const Eigen::MatrixXd &labels, const IKernel *kernel);
  RvmClassifier(const std::vector<T> &data, const std::vector<int> &labels, const IKernel<T> *kernel) :
   eps(0.01),
   kernel_(kernel),
   training_data_(data),
   training_labels_(Eigen::VectorXd::Zero(labels.size())),
   x_m_(training_data_),
   w_(Eigen::MatrixXd::Zero(data.size(), 1)),
   alpha_(Eigen::MatrixXd::Ones(data.size(), 1)) {
    BOOST_ASSERT(data.size() == labels.size());
    phi_samples_ = ComputePhi(training_data_);

    for (size_t i=0; i<labels.size(); i++) {
      training_labels_(i) = labels[i];
    }
  }

  const std::vector<T>& GetRelevanceVectors() const {
    return x_m_;
  }

  const Eigen::MatrixXd& GetWeights() const {
    return w_;
  }

  virtual size_t NumRelevanceVectors() const {
    return GetRelevanceVectors().size();
  }

  std::vector<double> PredictLabels(const std::vector<T> &samples) const {
    Eigen::MatrixXd phi = ComputePhi(samples);

    Eigen::MatrixXd exponent = phi * w_;
    auto exp = exponent.array().exp();
    auto prob = (1 + exp).inverse();

    // map to std::vector
    std::vector<double> res;
    res.resize(samples.size());
    Eigen::VectorXd::Map(&res[0], samples.size()) = prob;

    return res;
  }

  void BatchSolve(int iterations) {
    for (int i=0; i<iterations; i++) {
      tr::Timer t;
      tr::Timer t_step;

      printf("Iteration %d / %d\n", i, iterations);

      t_step.Start();
      bool res = UpdateW();
      printf("\tUpdate w took %5.3f ms, LL now %f\n", t_step.GetMs(), ComputeLogLikelihood(w_));

      if (!res) {
        printf("\tUpdate w not successful, aborting\n");
        return;
      }

      t_step.Start();
      UpdateAlpha();
      printf("\tUpdate alpha took %5.3f ms\n", t_step.GetMs());

      // Prune parameters
      t_step.Start();
      PruneXm();
      printf("\tPruning took %5.3f ms, have %ld relevant vectors\n", t_step.GetMs(), NumRelevanceVectors());

      //printf("\tx_m is %ld x %ld\n", x_m_.rows(), x_m_.cols());
      //printf("\tw is %ld x %ld\n", w_.rows(), w_.cols());
      //printf("\tphi is %ld x %ld\n", phi_samples_.rows(), phi_samples_.cols());

      //printf("\nIteration took %5.3f ms\n", t.GetMs());
    }
  }

  virtual double ComputeLogLikelihood(const Eigen::MatrixXd &w, Eigen::MatrixXd *gradient = nullptr) const {
    Eigen::MatrixXd exponent = -phi_samples_ * w;
    auto exp = exponent.array().exp();
    Eigen::ArrayXXd y_n = (1 + exp).inverse();

    Eigen::ArrayXXd t_n = training_labels_.array();

    double c1 = (t_n * y_n.log()).sum();
    double c2 = ((1 - t_n) * (1 - y_n).log()).sum();

    Eigen::MatrixXd A = alpha_.col(0).asDiagonal();
    auto prod = -0.5 * w.transpose() * A * w;
    BOOST_ASSERT(prod.rows() == 1 && prod.cols() == 1);
    double c3 = prod(0, 0);

    if (gradient != nullptr) {
      //printf("getting grad...\n");
      //printf("\tx_m is %ld x %ld\n", x_m_.rows(), x_m_.cols());
      //printf("\tw is %ld x %ld\n", w.rows(), w.cols());
      //printf("\tphi is %ld x %ld\n", phi_samples_.rows(), phi_samples_.cols());
      //printf("\tt_n is %ld x %ld\n", t_n.rows(), t_n.cols());
      //printf("\ty_n is %ld x %ld\n", y_n.rows(), y_n.cols());

      //tr::Timer t;
      (*gradient) = phi_samples_.transpose() * (t_n - y_n).matrix() - A*w;
      //printf("got grad in %5.3f ms\n", t.GetMs());
    }

    return c1 + c2 + c3;
  }



 private:
  const IKernel<T> *kernel_;

  const double eps;

  std::vector<T> training_data_;
  Eigen::VectorXd training_labels_;

  std::vector<T> x_m_;
  Eigen::MatrixXd w_;
  Eigen::MatrixXd alpha_;

  Eigen::SparseMatrix<double> phi_samples_;

  Eigen::SparseMatrix<double> ComputePhi(const std::vector<T> &data) const {
    //Eigen::MatrixXd phi(data.rows(), x_m_.rows());
    Eigen::SparseMatrix<double> phi;
    phi.conservativeResize(data.size(), x_m_.size());

    for (size_t i=0; i<data.size(); i++) {
      T sample = data[i];
      for (size_t j=0; j<x_m_.size(); j++){
        T x_m = x_m_[j];

        // Sparsify
        double val = kernel_->Compute(sample, x_m);
        if (std::abs(val) > 1e-3) {
          phi.insert(i, j) = val;
        }
      }
    }

    return phi;
  }

  bool UpdateW() {
    ceres::GradientProblem problem(new RvmWeightFunction<T>(*this));
    ceres::GradientProblemSolver::Options options;
    //options.minimizer_progress_to_stdout = true;
    options.minimizer_progress_to_stdout = false;
    options.max_num_iterations = 100;
    options.max_num_line_search_step_size_iterations = 100;
    ceres::GradientProblemSolver::Summary summary;
    ceres::Solve(options, problem, w_.data(), &summary);

    //std::cout << summary.FullReport() << std::endl;
    return summary.IsSolutionUsable();
  }

  void UpdateAlpha() {
    //tr::Timer t;

    //t.Start();
    Eigen::MatrixXd exponent = -phi_samples_ * w_;
    auto exp = exponent.array().exp();
    Eigen::ArrayXXd y_n = (1 + exp).inverse();

    Eigen::ArrayXXd b = y_n * (1 - y_n);
    BOOST_ASSERT(b.cols() == 1);
    //printf("Took %5.3f ms to compute\n", t.GetMs());

    //t.Start();
    //Eigen::MatrixXd b_mat = b.matrix().col(0).asDiagonal();
    Eigen::SparseMatrix<double> b_mat;
    b_mat.conservativeResize(b.rows(), b.rows());
    for (int i=0; i<b.rows(); i++) {
      b_mat.insert(i, i) = b(i, 0);
    }

    //Eigen::MatrixXd A = alpha_.col(0).asDiagonal();
    Eigen::SparseMatrix<double> A;
    A.conservativeResize(alpha_.rows(), alpha_.rows());
    for (int i=0; i<alpha_.rows(); i++) {
      A.insert(i, i) = alpha_(i);
    }

    //Eigen::SparseMatrix<double> h_neg = (phi_samples_.transpose() * b_mat * phi_samples_ + A).eval();
    Eigen::SparseMatrix<double> h_neg = (phi_samples_.transpose() * b_mat * phi_samples_ + A);
    Eigen::MatrixXd identity(h_neg.rows(), h_neg.cols());
    identity.setIdentity();
    //printf("Took %5.3f ms to build problem\n", t.GetMs());

    //t.Start();
    //Eigen::SimplicialLLT<Eigen::SparseMatrix<double>, NaturalOrdering<int> > llt(h_neg);
    //Eigen::SimplicialLLT<Eigen::SparseMatrix<double>, Eigen::NaturalOrdering<int> > llt(h_neg);
    Eigen::SimplicialLLT<Eigen::SparseMatrix<double> > llt(h_neg);
    Eigen::MatrixXd cov = llt.solve(identity);
    //printf("Took %5.3f ms to invert %ld x %ld\n", t.GetMs(), cov.rows(), cov.cols());

    //t.Start();
    int n_x_m = NumRelevanceVectors();
    for (int i=0; i<n_x_m; i++) {
      double w2 = w_(i, 0) * w_(i, 0);
      alpha_(i, 0) = (1 - alpha_(i, 0) * cov(i, i)) / w2;
    }
    //printf("Took %5.3f ms to update\n", t.GetMs());
  }

  void PruneXm() {
    const double cutoff = 1e3;

    int n_x_m = NumRelevanceVectors();

    // Walk backwards
    int x_m_at = n_x_m - 1;

    while (x_m_at >= 0) {
      if (alpha_(x_m_at, 0) > cutoff) {
        // Prune
        RemoveColumn(&phi_samples_, x_m_at);
        //RemoveRow(&x_m_, x_m_at);
        x_m_.erase(x_m_.begin() + x_m_at);
        RemoveRow(&w_, x_m_at);
        RemoveRow(&alpha_, x_m_at);
      }

      x_m_at--;
    }

    // Rebuild phi_samples_
    //phi_samples_ = ComputePhi(training_data_);
  }

  static void RemoveRow(Eigen::MatrixXd *matrix, unsigned int rowToRemove) {
    unsigned int numRows = matrix->rows()-1;
    unsigned int numCols = matrix->cols();

    if (rowToRemove < numRows) {
      matrix->block(rowToRemove,0,numRows-rowToRemove,numCols) =
        matrix->block(rowToRemove+1,0,numRows-rowToRemove,numCols).eval();
    }

    matrix->conservativeResize(numRows,numCols);
  }

  static void RemoveColumn(Eigen::MatrixXd *matrix, unsigned int colToRemove) {
    unsigned int numRows = matrix->rows();
    unsigned int numCols = matrix->cols()-1;

    if (colToRemove < numCols) {
      matrix->block(0,colToRemove,numRows,numCols-colToRemove) =
        matrix->block(0,colToRemove+1,numRows,numCols-colToRemove).eval();
    }

    matrix->conservativeResize(numRows,numCols);
  }

  static void RemoveColumn(Eigen::SparseMatrix<double> *sp, unsigned int colToRemove) {
    Eigen::SparseMatrix<double> x;
    x.conservativeResize(sp->cols(), sp->cols()-1);
    for (unsigned int i=0; i<sp->cols()-1; i++) {
      int j = 0;
      if (i < colToRemove) {
        j = i;
      } else {
        j = i + 1;
      }
      x.insert(j, i) = 1;
    }

    (*sp) = (*sp) * x;
  }
};

template <class T>
class RvmRegression : public Rvm<T> {
 public:
  // data is n_samples X dimension
  // labels is n_samples X 1
  //Rvm(const Eigen::MatrixXd &data, const Eigen::MatrixXd &labels, const IKernel *kernel);
  RvmRegression(const std::vector<T> &data, const std::vector<double> &labels, const IKernel<T> *kernel) :
   eps(0.01),
   kernel_(kernel),
   training_data_(data),
   training_labels_(Eigen::VectorXd::Zero(labels.size())),
   x_m_(training_data_),
   w_(Eigen::MatrixXd::Zero(data.size(), 1)),
   alpha_(Eigen::MatrixXd::Ones(data.size(), 1)) {
    BOOST_ASSERT(data.size() == labels.size());

    for (size_t i=0; i<labels.size(); i++) {
      training_labels_(i) = labels[i];
    }
    var_ = LabelVariance() * 0.1;
  }

  const std::vector<T>& GetRelevanceVectors() const {
    return x_m_;
  }

  const Eigen::MatrixXd& GetWeights() const {
    return w_;
  }

  size_t NumRelevanceVectors() const {
    return GetRelevanceVectors().size();
  }

  std::vector<double> PredictLabels(const std::vector<T> &samples) const {
    Eigen::MatrixXd phi = ComputePhi(samples);
    Eigen::MatrixXd labels = phi * w_;

    // map to std::vector
    std::vector<double> res;
    res.resize(samples.size());
    Eigen::VectorXd::Map(&res[0], samples.size()) = labels;

    return res;
  }

  void SequentialSolve(const int max_iterations) {
    // make a target vector where +1 examples have value 1 and -1 examples
    // have a value of 0.

    /*! This is the convention for the active_bases variable in the function:
        - if (active_bases(i) >= 0) then
            - alpha(active_bases(i)) == the alpha value associated with sample x(i)
            - weights(active_bases(i)) == the weight value associated with sample x(i)
            - colm(phi, active_bases(i)) == the column of phi associated with sample x(i)
            - colm(phi, active_bases(i)) == kernel column i (from get_kernel_colum()) 
        - else
            - the i'th sample isn't in the model and notionally has an alpha of infinity and
              a weight of 0.
    !*/

    // set the initial values of these guys
    double var = var_;
    std::vector<long> active_bases(training_data_.size());
    std::fill(active_bases.begin(), active_bases.end(), -1);
    x_m_.clear();
    long first_basis = PickVector();
    x_m_.push_back(training_data_[first_basis]);
    active_bases[first_basis] = 0;
    std::cout << "Computing Phi " << first_basis << "\n";
    phi_samples_ = GetKernelColumn(first_basis);
    alpha_.resize(1,1);
    alpha_(0,0) = phi_samples_.squaredNorm()/((phi_samples_.transpose()*training_labels_).squaredNorm()/phi_samples_.squaredNorm()-var);
    w_.resize(1,1);
    w_(0,0) = 1;
    Eigen::MatrixXd prev_alpha = alpha_;
    Eigen::MatrixXd prev_w = w_;

    // now declare a bunch of other variables we will be using below
    Eigen::MatrixXd Q(training_data_.size(),1);
    Eigen::MatrixXd S(training_data_.size(),1);

    bool search_all_alphas = false;
    unsigned long ticker = 0;
    unsigned long iteration = 0;
    const unsigned long rounds_of_narrow_search = 100;

    while (true)
    {
        iteration++;
        std::cout << "Iteration: " << iteration << std::endl;
        printf("LL: %5.3f Number Relevance Vectors: %d\n", ComputeLogLikelihood(GetWeights()), NumRelevanceVectors());
        if(iteration>max_iterations)
          break;

        // Compute optimal weights and sigma for current alpha using equation 6. 
        Eigen::MatrixXd sigma = (phi_samples_.transpose()*phi_samples_)/var;
        for (long r = 0; r < alpha_.rows(); ++r)
            sigma(r,r) += alpha_(r);
        sigma = sigma.inverse();
        w_ = (sigma*(phi_samples_.transpose()*training_labels_))/var;

        // check if we should do a full search for the best alpha to optimize
        if (ticker == rounds_of_narrow_search)
        {
            // if the current alpha and weights are equal to what they were
            // at the last time we were about to start a wide search then
            // we are done.
            if (Equal(prev_alpha, alpha_) && Equal(prev_w, w_)) {
                std::cout << "No alpha change\n";
                break;
            }

            prev_alpha = alpha_;
            prev_w = w_;
            search_all_alphas = true;
            ticker = 0;
        }
        else
        {
            search_all_alphas = false;
        }
        ++ticker;

        // compute S and Q using equations 24 and 25 (tempv = phi*sigma*trans(phi)*B*t)
        //Eigen::SparseMatrix<double> sparse_sigma = sigma.sparseView(1, 1e-6);
        //Eigen::SparseMatrix<double> sparse_labels = training_labels_.sparseView(1, 1e-6);
        std::cout << "temp v " << phi_samples_.rows() << " " << sigma.cols() << " \n";
        Eigen::MatrixXd tempv2 = phi_samples_.transpose()*training_labels_/(var);
        //Eigen::SparseMatrix<double> tempv = (phi_samples_*sparse_sigma*phi_samples_.transpose()*sparse_labels)/var;
        for (long i = 0; i < S.rows(); ++i)
        {
            // if we are currently limiting the search for the next alpha to update
            // to the set in the active set then skip a non-active vector.
            if ( active_bases[i] == -1)
                continue;

            // get the column for this sample out of the kernel matrix.  If it is 
            // something in the active set then just get it right out of phi, otherwise 
            // we have to calculate it.
            Eigen::SparseMatrix<double> K_col;
            if (active_bases[i] != -1) {
              K_col = phi_samples_.col(active_bases[i]);
            } else {
              K_col = GetKernelColumn(i);
            }

            // tempv2 = trans(phi_m)*B
            //Eigen::SparseMatrix<double> tempv2 = K_col.transpose()/var;  
            //Eigen::SparseMatrix<double> tempv3 = tempv2*phi_samples_;
            //double s = 0;
            //double q = 0;
            //for(int it = 0; it < S.rows(); it++) {
            //  s += (K_col.row(it).dot(K_col.row(it)))/var
            //    -(K_col.row(it).dot(phi_samples_.row(it)*sigma*(phi_samples_.row(it)).transpose()*K_col.row(it).transpose()))/(var*var);
            //  q += K_col.row(it).dot(training_labels_.row(it))/var
            //    -(K_col.row(it).dot(phi_samples_.row(it)*sigma*(phi_samples_.row(it)).transpose()*training_labels_.row(it).transpose()))/(var*var);
            //}

            Eigen::MatrixXd tempv1 = K_col.transpose()*phi_samples_/(var);

            S.block(i,0,1,1) = K_col.transpose()*K_col/(var)-tempv1*sigma*tempv1.transpose();
            Q.block(i,0,1,1) = K_col.transpose()*training_labels_/(var)-tempv1*sigma*tempv2;

            //S.block(i,0,1,1) = tempv2*K_col - tempv3*sparse_sigma*Eigen::SparseMatrix<double>(tempv3.transpose());
            //Q.block(i,0,1,1) = tempv2*sparse_labels - tempv2*tempv;
            //std::cout << "S: " << S(i,0) << " Q: " << Q(i,0) << std::endl;
            //S.block(i,0,1,1) = (K_col.transpose()/var)*K_col - (K_col.transpose()/var)*phi_samples_*sparse_sigma*phi_samples_.transpose()*(K_col/var);
            //Q.block(i,0,1,1) = (K_col.transpose()/var)*sparse_labels - (K_col.transpose()/var)*((phi_samples_*sparse_sigma*phi_samples_.transpose()*sparse_labels)/var);
        }

        const long selected_idx = NextBestAlpha(S,Q,active_bases, search_all_alphas);
        std::cout << "Computed Next best alpha " << selected_idx << " active basis " << active_bases[selected_idx] << "\n";

        // if find_next_best_alpha_to_update didn't find any good alpha to update
        if (selected_idx == -1)
        {
            if (search_all_alphas == false)
            {
                // jump us to where search_all_alphas will be set to true and try again
                ticker = rounds_of_narrow_search;
                continue;
            }
            else
            {
                // we are really done so quit the main loop
                break;
            }
        }

        // recompute the variance
        var = (training_labels_ - phi_samples_*w_).squaredNorm()
          /(static_cast<double>(training_data_.size()) -static_cast<double>( w_.rows())
              + (alpha_.transpose()*sigma.diagonal())(0,0));

        // next we update the selected alpha.

        // if the selected alpha is in the active set
        if (active_bases[selected_idx] >= 0)
        {
            const long idx = active_bases[selected_idx];
            const double s = alpha_(idx,0)*S(selected_idx,0)/(alpha_(idx,0) - S(selected_idx,0));
            const double q = alpha_(idx,0)*Q(selected_idx,0)/(alpha_(idx,0) - S(selected_idx,0));

            if (q*q-s > 0)
            {
                double old = alpha_(idx,0);
                // reestimate the value of alpha
                alpha_(idx,0) = s*s/(q*q-s);
                std::cout << "basis" << idx << " index " << selected_idx <<  " s: " << s << " q: " << q << std::endl;
                std::cout << "Restimate the Value of Alpha, old: " << 1/old << " new " << 1/alpha_(idx,0) << std::endl;

            }
            else
            {
                double old = alpha_(idx,0);
                std::cout << "Remove selected Alpha Value From model, value was " << 1.0/old << "\n";
                // the new alpha value is infinite so remove the selected alpha from our model
                active_bases[selected_idx] = -1;
                RemoveColumn(&phi_samples_, idx);
                RemoveRow(&w_, idx);
                RemoveRow(&alpha_, idx);
                x_m_.erase(x_m_.begin()+idx);

                // fix the index values in active_bases
                for (long i = 0; i < active_bases.size(); ++i)
                {
                    if (active_bases[i] > idx)
                    {
                        active_bases[i] -= 1;
                    }
                }
            }
        }
        else
        {
            Eigen::SparseMatrix<double> K_col = GetKernelColumn(selected_idx);
            //S.block(selected_idx,0,1,1) = (K_col.transpose()/var)*K_col - (K_col.transpose()/var)*phi_samples_*sparse_sigma*phi_samples_.transpose()*(K_col/var);
            //Q.block(selected_idx,0,1,1) = (K_col.transpose()/var)*sparse_labels - (K_col.transpose()/var)*((phi_samples_*sparse_sigma*phi_samples_.transpose()*sparse_labels)/var);
            //Eigen::SparseMatrix<double> tempv2 = K_col.transpose()/var;  
            //Eigen::SparseMatrix<double> tempv3 = tempv2*phi_samples_;
            //S.block(selected_idx,0,1,1) = tempv2*K_col - tempv3*sparse_sigma*Eigen::SparseMatrix<double>(tempv3.transpose());
            //Q.block(selected_idx,0,1,1) = tempv2*sparse_labels - tempv2*tempv;
            Eigen::MatrixXd tempv1 = K_col.transpose()*phi_samples_/(var);

            S.block(selected_idx,0,1,1) = K_col.transpose()*K_col/(var)-tempv1*sigma*tempv1.transpose();
            Q.block(selected_idx,0,1,1) = K_col.transpose()*training_labels_/(var)-tempv1*sigma*tempv2;
            std::cout << "S: " << S(selected_idx,0) << " Q: " << Q(selected_idx,0) << std::endl;
            const double s = S(selected_idx,0);
            const double q = Q(selected_idx,0);

            if (q*q-s > 0)
            {
                // add the selected alpha to our model

                active_bases[selected_idx] = phi_samples_.cols();
                x_m_.push_back(training_data_[selected_idx]);

                // update alpha
                Eigen::MatrixXd temp_alpha(alpha_.rows()+1,1);
                temp_alpha.topLeftCorner(alpha_.rows(), alpha_.cols()) = alpha_;
                temp_alpha(alpha_.rows(),0) = s*s/(q*q-s);
                alpha_ = temp_alpha;

                // update weights
                Eigen::MatrixXd temp_w(w_.rows()+1,1);
                temp_w.topLeftCorner(w_.rows(),w_.cols()) = w_;
                temp_w(w_.rows(),0) = 0;
                w_ = temp_w;

                // update phi by adding the new sample's kernel matrix column in as one of phi's columns
                Eigen::SparseMatrix<double> temp_phi;
                temp_phi.conservativeResize(phi_samples_.rows(), phi_samples_.cols()+1);
                temp_phi.leftCols(phi_samples_.cols()) = phi_samples_;
                temp_phi.col(phi_samples_.cols()) = GetKernelColumn(selected_idx);
                phi_samples_ = temp_phi;
            }
        }


    } // end while(true).  So we have converged on the final answer.
    std::cout << "Phi Size: " << phi_samples_.cols() << " w size: " << w_.rows() << std::endl;
  }

  void BatchSolve(int iterations) {
    std::cout << "Computing Phi\n";
    phi_samples_ = ComputePhi(training_data_);
    std::cout << "Computing Done\n";
    for (int i=0; i<iterations; i++) {
      tr::Timer t;
      tr::Timer t_step;

      printf("Iteration %d / %d\n", i, iterations);

      t_step.Start();
      bool res = UpdateW();
      printf("\tUpdate w took %5.3f ms, LL now %f\n", t_step.GetMs(), ComputeLogLikelihood(w_));

      if (!res) {
        printf("\tUpdate w not successful, aborting\n");
        return;
      }

      t_step.Start();
      UpdateAlpha();
      printf("\tUpdate alpha took %5.3f ms\n", t_step.GetMs());

      // Prune parameters
      t_step.Start();
      PruneXm();
      printf("\tPruning took %5.3f ms, have %ld relevant vectors\n", t_step.GetMs(), NumRelevanceVectors());

      //printf("\tx_m is %ld x %ld\n", x_m_.rows(), x_m_.cols());
      //printf("\tw is %ld x %ld\n", w_.rows(), w_.cols());
      //printf("\tphi is %ld x %ld\n", phi_samples_.rows(), phi_samples_.cols());

      //printf("\nIteration took %5.3f ms\n", t.GetMs());
    }
  }

  double ComputeLogLikelihood(const Eigen::MatrixXd &w, Eigen::MatrixXd *gradient = nullptr) const {
    Eigen::MatrixXd err = training_labels_-phi_samples_ * w;

    if (gradient != nullptr) {
      //printf("getting grad...\n");
      //printf("\tx_m is %ld x %ld\n", x_m_.rows(), x_m_.cols());
      //printf("\tw is %ld x %ld\n", w.rows(), w.cols());
      //printf("\tphi is %ld x %ld\n", phi_samples_.rows(), phi_samples_.cols());
      //printf("\tt_n is %ld x %ld\n", t_n.rows(), t_n.cols());
      //printf("\ty_n is %ld x %ld\n", y_n.rows(), y_n.cols());

      //tr::Timer t;
      (*gradient) = (1.0/var_*err.transpose()*phi_samples_).transpose();
      //printf("got grad in %5.3f ms\n", t.GetMs());
    }

    return err.squaredNorm()/(-2.0*var_);
  }

  class RvmRegressionDecisionFunction {
    public:
      RvmRegressionDecisionFunction( const std::vector<T> x_m, const Eigen::MatrixXd w, const IKernel<T>  *kernel ) :
        x_m_(x_m),
        w_(w),
        kernel_(kernel) {};

      double PredictLabel(const T& sample) const {
        //std::cout << "Predict Label\n " << sample << "\n";
        Eigen::MatrixXd phi = ComputePhi(sample);
        //std::cout << "X\n " << x_m_[0] << "\nw\n" << w_  << "\nPhi\n" << phi << "\n";
        double val = (phi *w_)(0,0);
       // std::cout << "val " << val << std::endl;
        return val;
      }

      void ComputeGradient(const T &sample, Eigen::Vector3d& dt, Eigen::Matrix3d& dR) const {
        dt = Eigen::Vector3d::Zero();
        dR = Eigen::Matrix3d::Zero();
        double r2 = kernel_->GetGradient();

        Eigen::Vector3d temp = Eigen::Vector3d::Zero();
        for( size_t j = 0; j<x_m_.size(); j++) {
          temp = w_(j,0) * kernel_->Compute(sample, x_m_[j])/r2 * (x_m_[j] - sample);
          dt -= temp;
          dR -= temp * sample.transpose();
        }
        dt *= 2.0;
        dR *= 2.0;
      }

    private:
      const IKernel<T> *kernel_;
      const std::vector<T> x_m_;
      const Eigen::MatrixXd w_;

      Eigen::MatrixXd ComputePhi(const T &data) const {
        //Eigen::MatrixXd phi(data.rows(), x_m_.rows());
        Eigen::MatrixXd phi(1, x_m_.size());

        for (size_t j=0; j<x_m_.size(); j++){
          const T& x_m = x_m_[j];
          //std::cout << "data\n" << data << "\n x_m\n" << x_m << "\nsq nm\n" << (data-x_m).squaredNorm() << "\n exp\n" << std::exp(-(data-x_m).squaredNorm()/(5.5*5.5)) << std::endl;
          //std::cout << "Before Compute\n";
          phi(0,j) = kernel_->Compute(data, x_m);
          //std::cout << "phi\n" << phi(0,j) << std::endl;
        }
        return phi;
      }
  };

  std::unique_ptr<RvmRegressionDecisionFunction> GetDecisionFunction() {
    return std::make_unique<RvmRegressionDecisionFunction>(x_m_, w_, kernel_);
  }

 private:
  const IKernel<T> *kernel_;

  const double eps;
  double var_;

  std::vector<T> training_data_;
  Eigen::VectorXd training_labels_;

  std::vector<T> x_m_;
  Eigen::MatrixXd w_;
  Eigen::MatrixXd alpha_;

  Eigen::SparseMatrix<double> phi_samples_;


  Eigen::SparseMatrix<double> ComputePhi(const std::vector<T> &data) const {
    //Eigen::MatrixXd phi(data.rows(), x_m_.rows());
    Eigen::SparseMatrix<double> phi;
    phi.conservativeResize(data.size(), x_m_.size());

    for (size_t i=0; i<data.size(); i++) {
      T sample = data[i];
      for (size_t j=0; j<x_m_.size(); j++){
        T x_m = x_m_[j];

        // Sparsify
        double val = kernel_->Compute(sample, x_m);
        if (std::abs(val) > 1e-5) {
          phi.insert(i, j) = val;
        }
      }
    }

    return phi;
  }

  Eigen::SparseVector<double> GetKernelColumn( long idx ) const {
    Eigen::SparseVector<double> out;
    out.resize(training_data_.size());
    for (long i = 0; i < training_data_.size(); i++) {
      double val = kernel_->Compute(training_data_[i], training_data_[idx]);
      if(std::abs(val) > 1e-5) {
        out.insert(i) = val;
      }
    }
    return out;
  }


  bool UpdateW() {
    ceres::GradientProblem problem(new RvmWeightFunction<T>(*this));
    ceres::GradientProblemSolver::Options options;
    //options.minimizer_progress_to_stdout = true;
    options.minimizer_progress_to_stdout = false;
    options.max_num_iterations = 100;
    options.max_num_line_search_step_size_iterations = 100;
    ceres::GradientProblemSolver::Summary summary;
    ceres::Solve(options, problem, w_.data(), &summary);

    //std::cout << summary.FullReport() << std::endl;
    return summary.IsSolutionUsable();
  }

  void UpdateAlpha() {
    //tr::Timer t;

    //t.Start();
    Eigen::MatrixXd exponent = -phi_samples_ * w_;
    auto exp = exponent.array().exp();
    Eigen::ArrayXXd y_n = (1 + exp).inverse();

    Eigen::ArrayXXd b = y_n * (1 - y_n);
    BOOST_ASSERT(b.cols() == 1);
    //printf("Took %5.3f ms to compute\n", t.GetMs());

    //t.Start();
    //Eigen::MatrixXd b_mat = b.matrix().col(0).asDiagonal();
    Eigen::SparseMatrix<double> b_mat;
    b_mat.conservativeResize(b.rows(), b.rows());
    for (int i=0; i<b.rows(); i++) {
      b_mat.insert(i, i) = b(i, 0);
    }

    //Eigen::MatrixXd A = alpha_.col(0).asDiagonal();
    Eigen::SparseMatrix<double> A;
    A.conservativeResize(alpha_.rows(), alpha_.rows());
    for (int i=0; i<alpha_.rows(); i++) {
      A.insert(i, i) = alpha_(i);
    }

    //Eigen::SparseMatrix<double> h_neg = (phi_samples_.transpose() * b_mat * phi_samples_ + A).eval();
    Eigen::SparseMatrix<double> h_neg = (phi_samples_.transpose() * b_mat * phi_samples_ + A);
    Eigen::MatrixXd identity(h_neg.rows(), h_neg.cols());
    identity.setIdentity();
    //printf("Took %5.3f ms to build problem\n", t.GetMs());

    //t.Start();
    //Eigen::SimplicialLLT<Eigen::SparseMatrix<double>, NaturalOrdering<int> > llt(h_neg);
    //Eigen::SimplicialLLT<Eigen::SparseMatrix<double>, Eigen::NaturalOrdering<int> > llt(h_neg);
    Eigen::SimplicialLLT<Eigen::SparseMatrix<double> > llt(h_neg);
    Eigen::MatrixXd cov = llt.solve(identity);
    //printf("Took %5.3f ms to invert %ld x %ld\n", t.GetMs(), cov.rows(), cov.cols());

    //t.Start();
    int n_x_m = NumRelevanceVectors();
    for (int i=0; i<n_x_m; i++) {
      double w2 = w_(i, 0) * w_(i, 0);
      alpha_(i, 0) = (1 - alpha_(i, 0) * cov(i, i)) / w2;
    }
    //printf("Took %5.3f ms to update\n", t.GetMs());
  }

  void PruneXm() {
    const double cutoff = 1e3;

    int n_x_m = NumRelevanceVectors();

    // Walk backwards
    int x_m_at = n_x_m - 1;

    while (x_m_at >= 0) {
      if (alpha_(x_m_at, 0) > cutoff) {
        // Prune
        RemoveColumn(&phi_samples_, x_m_at);
        //RemoveRow(&x_m_, x_m_at);
        x_m_.erase(x_m_.begin() + x_m_at);
        RemoveRow(&w_, x_m_at);
        RemoveRow(&alpha_, x_m_at);
      }

      x_m_at--;
    }

    // Rebuild phi_samples_
    //phi_samples_ = ComputePhi(training_data_);
  }

  static void RemoveRow(Eigen::MatrixXd *matrix, unsigned int rowToRemove) {
    unsigned int numRows = matrix->rows()-1;
    unsigned int numCols = matrix->cols();

    if (rowToRemove < numRows) {
      matrix->block(rowToRemove,0,numRows-rowToRemove,numCols) =
        matrix->block(rowToRemove+1,0,numRows-rowToRemove,numCols).eval();
    }

    matrix->conservativeResize(numRows,numCols);
  }

  static void RemoveColumn(Eigen::MatrixXd *matrix, unsigned int colToRemove) {
    unsigned int numRows = matrix->rows();
    unsigned int numCols = matrix->cols()-1;

    if (colToRemove < numCols) {
      matrix->block(0,colToRemove,numRows,numCols-colToRemove) =
        matrix->block(0,colToRemove+1,numRows,numCols-colToRemove).eval();
    }

    matrix->conservativeResize(numRows,numCols);
  }

  static void RemoveColumn(Eigen::SparseMatrix<double> *sp, unsigned int colToRemove) {
    Eigen::SparseMatrix<double> x;
    x.conservativeResize(sp->cols(), sp->cols()-1);
    for (unsigned int i=0; i<sp->cols()-1; i++) {
      int j = 0;
      if (i < colToRemove) {
        j = i;
      } else {
        j = i + 1;
      }
      x.insert(j, i) = 1;
    }

    (*sp) = (*sp) * x;
  }

  double LabelVariance() {
    double sum = 0;
    double sum_sq = 0;
    for(long i = 1; i< training_labels_.rows(); i++) {
      sum += training_labels_(i,0);
      sum_sq += training_labels_(i,0) * training_labels_(i,0);
    }
    return (sum_sq - sum*sum/static_cast<double>(training_labels_.rows()))
      /static_cast<double>(training_labels_.rows()-1);
  }

  long PickVector() {
    std::vector<std::tuple<double, int>> vector_queue;

    std::vector<int> v(training_data_.size());
    std::iota (std::begin(v), std::end(v), 0);

    // Limit how many we check
    if( v.size() > 999) {
      std::random_device rd;
      std::mt19937 g(rd());
      std::shuffle(std::begin(v), std::end(v), g);
      v.erase(v.begin()+1000, v.end());
    }

    for ( auto r : v) {
      Eigen::SparseVector<double> k_col = GetKernelColumn(r);
      double temp = k_col.dot(training_labels_);
      temp  = temp*temp / k_col.squaredNorm();
      vector_queue.push_back(std::make_tuple(temp,r));
    }
    std::sort(vector_queue.begin(), vector_queue.end(), [](std::tuple<double,int> left, std::tuple<double, int> right) {return std::get<0>(left) < std::get<0>(right);});
    auto tup = vector_queue.back();
    bool keep_searching = true;
    while( keep_searching) {
      keep_searching = false;
      tup = vector_queue.back();
      vector_queue.pop_back();
      for ( const auto x : x_m_) {
        if ( (x-training_data_[std::get<1>(tup)]).norm() < 0.001 ) {
          keep_searching = true;
        }
      }
    }

    return std::get<1>(tup);
  }

  long NextBestAlpha( const Eigen::MatrixXd& S, const Eigen::MatrixXd& Q, const std::vector<long>& active_bases,
      const bool search_all_alphas) {
  long selected_idx = -1;
  double greatest_improvement = -1;
  for (long i = 0; i < S.rows(); ++i)
  {
      double value = -1;

      // if i is currently in the active set
      if (active_bases[i] >= 0)
      {
          const long idx = active_bases[i];
          const double s = alpha_(idx,0)*S(i,0)/(alpha_(idx,0) - S(i,0));
          const double q = alpha_(idx,0)*Q(i,0)/(alpha_(idx,0) - S(i,0));
          //std::cout << "basis " << idx << " index " << i <<  " s: " << s << " q: " << q << std::endl;

          if (q*q-s > 0)
          {
              // only update an existing alpha if this is a narrow search
              if (search_all_alphas == false)
              {
                  // choosing this sample would mean doing an update of an 
                  // existing alpha value.
                  double new_alpha = s*s/(q*q-s);
                  double cur_alpha = alpha_(idx,0);
                  new_alpha = 1/new_alpha;
                  cur_alpha = 1/cur_alpha;

                  // from equation 32 in the Tipping paper 
                  value = Q(i,0)*Q(i,0)/(S(i,0) +  1/(new_alpha - cur_alpha) ) - 
                      std::log(1 + S(i,0)*(new_alpha - cur_alpha));
                  //std::cout << "Index: " << i << " Value: " << value << " new alpha: " << new_alpha << " current alpha: " << cur_alpha << " selected_idx: " << selected_idx << std::endl;
              }

          }
          // we only pick an alpha to remove if this is a wide search and it wasn't one of the recently added ones 
          else if (search_all_alphas && idx+2 < alpha_.size() )  
          {
              // choosing this sample would mean the alpha value is infinite 
              // so we would remove the selected sample from our model.

              // from equation 37 in the Tipping paper 
            std::cout << "from equation 37 in the tipping paper\n";
              value = Q(i,0)*Q(i,0)/(S(i,0) - alpha_(idx,0)) - 
                  std::log(1-S(i,0)/alpha_(idx,0));

          }
      }
      else if (false)
      {
          const double s = S(i,0);
          const double q = Q(i,0);

          if (q*q-s > 0)
          {
              // choosing this sample would mean we would add the selected 
              // sample to our model.

              // from equation 27 in the Tipping paper 
              value = (Q(i,0)*Q(i,0)-S(i,0))/S(i,0) + std::log(S(i,0)/(Q(i,0)*Q(i,0)));
          }
      }

      if (value > greatest_improvement)
      {
          greatest_improvement = value;
          selected_idx = i;
      }
  }

  // If the greatest_improvement in marginal likelihood we would get is less
  // than our epsilon then report that there isn't anything else to do.  But
  // if it is big enough then return the selected_idx.
  if (greatest_improvement > eps)
      return selected_idx;
  else {
      int value = 0;
      int return_idx = 0;
      while (value != -1) {
         std::random_device rd;
         std::mt19937 gen(rd());
         std::uniform_int_distribution<> dis(0, S.rows() -1 );
         return_idx = dis(gen);
         value = active_bases[return_idx];
      }
      return return_idx;
  }
  }

  bool Equal(const Eigen::MatrixXd& a, const Eigen::MatrixXd& b) {
    if(a.rows()!=b.rows() || a.cols() != b.cols()) {
      return false;
    }

    for(long i = 0; i< a.rows(); i++) {
      for(long j = 0; j < a.cols(); j++) {
        if(std::abs(a(i,j)-b(i,j)) > eps) {
          return false;
        }
      }
    }
    return true;
  }

};


} // namespace bayesian_inference
} // namespace library
