# bayesian-inference

Bayesian Inference methods cc/python implementation

---

# Requirements

* Bazel: [https://github.com/bazelbuild/bazel/releases](https://github.com/bazelbuild/bazel/releases)
* Ceres: [https://github.com/ceres-solver/ceres-solver](https://github.com/ceres-solver/ceres-solver)
* Glog: [https://github.com/google/glog](https://github.com/google/glog)

---

# Install

```
git clone git@bitbucket.org:perl-sw/bayesian-inference.git
cd bayesian-inference/cc
bazel build //exec:rvm-exp
```

# Paper Parameters

## GICP-SE(3)
* K nearest neighbors = 20
* Function Tolerance = 1e-4
* Outter Iterations = 50

## MC-GICP
* K nearest neighbors = 20
* Function Tolerance = 1e-4
* Outter Iterations = 50
* Alpha = {1.0, 1.0, 1.0, 1.4}
* Lambda = 5

## NDT
* Function Tolerance = 1e-4
* Outter Iterations = 50

